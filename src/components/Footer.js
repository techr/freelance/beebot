import React from 'react';
import { useNavigate } from 'react-router-dom';
import '../sass/footer.scss';

const Footer = () => (
	<footer>
		<div className={'left'}>Безопасност на движението по пътищата © Иновации и консултиране</div>
		<div className={'right'}>
			<div
				role={'button'}
				tabIndex={'0'}
				onClick={() => window.electronAPI.openLink('https://bdp.innovateconsult.net')}
			>
				За играта
			</div>{' '}
			•{' '}
			<div role={'button'} tabIndex={'0'} onClick={() => useNavigate()('/Contacts')}>
				Контакти
			</div>{' '}
			•{' '}
			<div
				role={'button'}
				tabIndex={'0'}
				onClick={() => window.electronAPI.openLink('https://bdp.innovateconsult.net/terms-of-use')}
			>
				Поверителност и условия
			</div>
		</div>
	</footer>
);

export default Footer;
