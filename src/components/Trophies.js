import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import trophyActive from '../img/game/trophies/trophy-active.png';
import trophyInactive from '../img/game/trophies/trophy-inactive.png';
import { setLessonID } from '../redux/dbSlice';
import trophyStatuses from '../controllers/trophyStatuses';

const Trophies = () => {
	const [statuses, setStatuses] = useState([]);
	const dispatch = useDispatch();
	const lessonID = useSelector((state) => state.lessonID);
	useEffect(() => {
		setStatuses(trophyStatuses(lessonID));
	}, [lessonID]);
	return (
		<div className={'white-box trophies'}>
			<div>
				<img
					className={'pointer'}
					role={'button'}
					tabIndex={'0'}
					onClick={() => dispatch(setLessonID(0))}
					src={statuses[0] ? trophyActive : trophyInactive}
					alt={'Трофей'}
				/>
				<img
					className={'pointer'}
					role={'button'}
					tabIndex={'0'}
					onClick={() => dispatch(setLessonID(1))}
					src={statuses[1] ? trophyActive : trophyInactive}
					alt={'Трофей'}
				/>

				<img
					className={'pointer'}
					role={'button'}
					tabIndex={'0'}
					onClick={() => dispatch(setLessonID(2))}
					src={statuses[2] ? trophyActive : trophyInactive}
					alt={'Трофей'}
				/>
				<img
					className={'pointer'}
					role={'button'}
					tabIndex={'0'}
					onClick={() => dispatch(setLessonID(3))}
					src={statuses[3] ? trophyActive : trophyInactive}
					alt={'Трофей'}
				/>
				<img
					className={'pointer'}
					role={'button'}
					tabIndex={'0'}
					onClick={() => dispatch(setLessonID(4))}
					src={statuses[4] ? trophyActive : trophyInactive}
					alt={'Трофей'}
				/>
				<img
					className={'pointer'}
					role={'button'}
					tabIndex={'0'}
					onClick={() => dispatch(setLessonID(5))}
					src={statuses[5] ? trophyActive : trophyInactive}
					alt={'Трофей'}
				/>
				<img
					className={'pointer'}
					role={'button'}
					tabIndex={'0'}
					onClick={() => dispatch(setLessonID(6))}
					src={statuses[6] ? trophyActive : trophyInactive}
					alt={'Трофей'}
				/>
			</div>
			<div>
				<img
					className={'pointer'}
					role={'button'}
					tabIndex={'0'}
					onClick={() => dispatch(setLessonID(7))}
					src={statuses[7] ? trophyActive : trophyInactive}
					alt={'Трофей'}
				/>
				<img
					className={'pointer'}
					role={'button'}
					tabIndex={'0'}
					onClick={() => dispatch(setLessonID(8))}
					src={statuses[8] ? trophyActive : trophyInactive}
					alt={'Трофей'}
				/>
				<img
					className={'pointer'}
					role={'button'}
					tabIndex={'0'}
					onClick={() => dispatch(setLessonID(9))}
					src={statuses[9] ? trophyActive : trophyInactive}
					alt={'Трофей'}
				/>
				<img
					className={'pointer'}
					role={'button'}
					tabIndex={'0'}
					onClick={() => dispatch(setLessonID(10))}
					src={statuses[10] ? trophyActive : trophyInactive}
					alt={'Трофей'}
				/>
				<img
					className={'pointer'}
					role={'button'}
					tabIndex={'0'}
					onClick={() => dispatch(setLessonID(11))}
					src={statuses[11] ? trophyActive : trophyInactive}
					alt={'Трофей'}
				/>
				<img
					className={'pointer'}
					role={'button'}
					tabIndex={'0'}
					onClick={() => dispatch(setLessonID(12))}
					src={statuses[12] ? trophyActive : trophyInactive}
					alt={'Трофей'}
				/>
			</div>
		</div>
	);
};

export default Trophies;
