import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import getText from '../controllers/getText';

const Task = () => {
	const [currentPage, setCurrentPage] = useState(1);
	const [totalPages, setTotalPages] = useState(1);
	const [taskID, setTaskID] = useState(useSelector((state) => state.taskID));
	const lessonID = useSelector((state) => state.lessonID);
	const lang = useSelector((state) => state.lang);
	const [title, setTitle] = useState('');
	const [showTask, setShowTask] = useState(true);
	const [text, setText] = useState(undefined);
	let taskAudio = false;
	const [perPage, setPerPage] = useState({
		subtasks: [[]],
		answers: [[]]
	});
	let perPageNative = {
		subtasks: [[]],
		answers: [[]]
	};
	const pageCharacterLimits = { start: title.length, max: 230 };

	const updateTitle = () => {
		if (text) {
			const answerTitle = text.lessons[lessonID].tasks[taskID].answers.title;
			setTitle(
				`${taskID + 1}. ${
					// eslint-disable-next-line no-nested-ternary
					showTask
						? text.lessons[lessonID].tasks[taskID].title
						: answerTitle !== undefined
						? answerTitle
						: 'Решение'
				}`
			);
		}
	};

	useEffect(() => updateTitle(), [showTask]);

	const showPrevSubPage = () => {
		if (currentPage - 1 >= 1) {
			setCurrentPage((prevState) => prevState - 1);
		}
	};

	const showNextSubPage = () => {
		if (currentPage < totalPages && currentPage + 1 <= totalPages) {
			setCurrentPage((prevState) => prevState + 1);
		}
	};

	const getSubNavigation = () => (
		<div>
			<i role={'button'} tabIndex={'0'} className={'material-icons'} onClick={showPrevSubPage}>
				navigate_before
			</i>
			<div className={'pageStats'}>
				<span className={'current-page'}>{currentPage}</span>
				<span>/</span>
				<span className={'total-pages'}>{totalPages}</span>
			</div>
			<i role={'button'} tabIndex={'0'} className={'material-icons'} onClick={showNextSubPage}>
				navigate_next
			</i>
		</div>
	);

	const showPrevTask = () => {
		if (taskID - 1 >= 0) {
			setTaskID((prevState) => prevState - 1);
			setShowTask(true);
		}
	};

	const showNextTask = () => {
		if (taskID + 1 < text.lessons[lessonID].tasks.length) {
			setTaskID((prevState) => prevState + 1);
			setShowTask(true);
		}
	};

	const getNavigation = () => (
		<div>
			<i role={'button'} tabIndex={'0'} className={'material-icons'} onClick={showPrevTask}>
				skip_previous
			</i>
			<i role={'button'} tabIndex={'0'} className={'material-icons next'} onClick={showNextTask}>
				skip_next
			</i>
			{getSubNavigation()}
		</div>
	);

	const switchToAnswers = () => {
		if (!showTask) {
			setShowTask((prevState) => !prevState);
			setTotalPages(perPage.subtasks.length);
			setCurrentPage(1);
		} else {
			setShowTask((prevState) => !prevState);
			setTotalPages(perPage.answers.length);
			setCurrentPage(1);
		}
	};

	const generateNavigation = () => {
		if (text.lessons[lessonID].tasks.length > 1) {
			return <div className={'left'}>{getNavigation()}</div>;
		}
		return <div className={'left'}>{getSubNavigation()}</div>;
	};

	const playSound = (e) => {
		if (taskAudio !== false) {
			taskAudio.pause();
		}
		taskAudio = new Audio(
			`${process.env.PUBLIC_URL}/sounds/lessons/${lang}/${e.target.getAttribute('file')}`
		);

		taskAudio.play();
	};

	const subTask = (soundFile, spanText) => (
		<li>
			{soundFile !== false && (
				<i
					className={'material-icons'}
					role={'button'}
					tabIndex={'0'}
					// eslint-disable-next-line react/no-unknown-property
					file={soundFile}
					onClick={playSound}
				>
					record_voice_over
				</i>
			)}

			<span className={'bullet'}>&#8226;</span>
			<span>{spanText}</span>
		</li>
	);

	const getCurrentContent = (pageId) => {
		if (showTask) {
			return perPage.subtasks[pageId].map((subtask) => subtask);
		}
		return perPage.answers[pageId].map((subtask) => subtask);
	};

	const addSubPage = (page, taskText, index, isTask) => {
		const property = isTask ? 'subtasks' : 'answers';

		if (perPageNative[property][page.id].length === 2) {
			// eslint-disable-next-line no-param-reassign
			page.id += 1;
			// eslint-disable-next-line no-param-reassign
			page.length = pageCharacterLimits.start + taskText.length;
		} else if (page.length + taskText.length > pageCharacterLimits.max) {
			if (page.hasBeenChanged) {
				// eslint-disable-next-line no-param-reassign
				page.id += 1;
			}
			// eslint-disable-next-line no-param-reassign
			page.length = pageCharacterLimits.start + taskText.length;
			// eslint-disable-next-line no-param-reassign
			page.hasBeenChanged = true;
		} else {
			// eslint-disable-next-line no-param-reassign
			page.length += taskText.length;
		}
		if (perPageNative[property][page.id] === undefined) {
			perPageNative[property][page.id] = [];
		}
		perPageNative[property][page.id].push(
			subTask(isTask ? `${lessonID + 1}.${taskID + 1}.${index + 1}.ogg` : false, taskText)
		);
		return page;
	};

	const generateSubPages = () => {
		if (!text) return;
		perPageNative = {
			subtasks: [[]],
			answers: [[]]
		};
		if (text.lessons[lessonID].tasks[taskID].text !== undefined) {
			let subTaskPage = {
				id: 0,
				length: pageCharacterLimits.start,
				hasBeenChanged: false
			};
			text.lessons[lessonID].tasks[taskID].text.forEach((taskText, index) => {
				subTaskPage = addSubPage(subTaskPage, taskText, index, true);
			});
		}
		if (text.lessons[lessonID].tasks[taskID].answers.content !== undefined) {
			let answersPage = {
				id: 0,
				length: pageCharacterLimits.start,
				hasBeenChanged: false
			};
			text.lessons[lessonID].tasks[taskID].answers.content.forEach((answerText, index) => {
				answersPage = addSubPage(answersPage, answerText, index, false);
			});
		}
		setCurrentPage(1);
		setTotalPages(perPageNative.subtasks.length);
		setPerPage(perPageNative);
	};

	useEffect(() => {
		updateTitle();
		generateSubPages();
	}, [text, lessonID, taskID]);

	useEffect(() => {
		getText().then((result) => setText(result));
	}, []);

	return text ? (
		<div className={'white-box lesson'}>
			<div className={'content'}>
				<h3 className={showTask ? '' : 'answer'}>{title}</h3>
				<ul>{getCurrentContent(currentPage - 1, lessonID)}</ul>
			</div>
			<nav>
				{generateNavigation(perPage, lessonID)}
				<div className={'right'}>
					<i className={'material-icons'} role={'button'} tabIndex={'0'} onClick={switchToAnswers}>
						check
					</i>
				</div>
			</nav>
		</div>
	) : (
		<div />
	);
};

export default Task;
