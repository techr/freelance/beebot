import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import Task from './Task';
import Trophies from './Trophies';
import ribbon from '../img/game/ribbon.png';
import getText from '../controllers/getText';

const lesson = () => {
	const [text, setText] = useState(undefined);
	const lessonID = useSelector((state) => state.lessonID);
	useEffect(() => {
		getText().then((result) => setText(result));
	}, []);
	return text ? (
		<div>
			<div className={'ribbon'}>
				<img src={ribbon} alt={'Ribbon'} />
				<h1 className={'title'}>{text.lessons[lessonID].title}</h1>
			</div>
			<Trophies />
			<Task />
		</div>
	) : (
		<div />
	);
};

export default lesson;
