import React from 'react';
import { Navigate } from 'react-router-dom';
import { useSelector } from 'react-redux';

export default () => <Navigate to={useSelector((state) => state.nextPage)} />;
