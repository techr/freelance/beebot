import React from 'react';
import { MemoryRouter as Router, Routes, Route } from 'react-router-dom';
import { TouchBackend } from 'react-dnd-touch-backend';
import { DndProvider } from 'react-dnd';
import SecondaryLocation from './SecondaryLocation';
import Home from './pages/Home';
import Game from './pages/Game';
import Contents from './pages/Contents';
import Contacts from './pages/Contacts';
import Terms from './pages/Terms';
import Privacy from './pages/Privacy';
import InitialLocation from './InitialLocation';
import Activation from './pages/Activation';

// eslint-disable-next-line func-names
export default () => (
	<DndProvider backend={TouchBackend} options={{ enableMouseEvents: true, preview: true }}>
		<Router>
			<Routes>
				<Route path={'/Home'} element={<Home />} />
				<Route path={'/Game'} element={<Game />} />
				<Route path={'/Contents'} element={<Contents />} />
				<Route path={'/Contacts'} element={<Contacts />} />
				<Route path={'/Terms'} element={<Terms />} />
				<Route path={'/Privacy'} element={<Privacy />} />
				<Route path={'/Activation'} element={<Activation />} />
				<Route path={'/SecondaryLocation'} element={<SecondaryLocation />} />
				<Route path={'/index.html'} element={<InitialLocation />} />
				<Route exact path={'/'} element={<InitialLocation />} />
			</Routes>
		</Router>
	</DndProvider>
);
