/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit';

const initialState = {
	character: 'aaa',
	expiration: null,
	mac: undefined,
	nextPage: '',
	lessonID: 0,
	taskID: 0,
	lang: 'bg'
};

const dbSlice = createSlice({
	name: 'dbSlice',
	initialState,
	reducers: {
		setCharacter: (state, action) => {
			state.character = action.payload;
		},
		setExpiration: (state, action) => {
			state.expiration = action.payload;
		},
		setMAC: (state, action) => {
			state.mac = action.payload;
		},
		setNextPage: (state, action) => {
			state.nextPage = action.payload;
		},
		setLessonID: (state, action) => {
			state.lessonID = action.payload;
			state.taskID = 0;
		},
		setTaskID: (state, action) => {
			state.taskID = action.payload;
		},
		setLang: (state, action) => {
			state.lang = action.payload;
		}
	}
});

export const { setCharacter, setExpiration, setMAC, setNextPage, setLessonID, setTaskID, setLang } =
	dbSlice.actions;
export default dbSlice.reducer;
