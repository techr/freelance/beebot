import { configureStore } from '@reduxjs/toolkit';
import storage from 'redux-persist/lib/storage';
import { persistReducer, persistStore, getStoredState } from 'redux-persist';
import storageSession from 'redux-persist/lib/storage/session';
import dbSlice from './dbSlice';

const persistConfig = {
	key: 'root',
	storage,
	storageSession
};

const persistedReducer = persistReducer(persistConfig, dbSlice);

export const store = configureStore({
	reducer: persistedReducer,
	devTools: process.env.NODE_ENV !== 'production'
});

export const persistor = persistStore(store);

export async function getManualRehydration() {
	const state = await getStoredState(persistConfig);
	return { type: 'persist/REHYDRATE', payload: state };
}
