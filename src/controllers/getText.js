import lessons from '../text-content/lessons';
import ui from '../text-content/ui';
import { store, getManualRehydration } from '../redux/store';

const getText = () =>
	getManualRehydration()
		.then((rehydration) => store.dispatch(rehydration))
		.then(() => {
			const text = {};
			const langSetting = store.getState().lang;
			if (langSetting !== 'en') {
				text.lessons = lessons.bg;
				text.ui = ui.bg;
				text.lang = 'bg';
			} else {
				text.lessons = lessons.en;
				text.ui = ui.en;
				text.lang = 'en';
			}
			return text;
		});
export default getText;
