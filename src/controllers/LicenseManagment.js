import $ from 'jquery';
import config from '../config';
import { store } from '../redux/store';
import { setExpiration, setMAC } from '../redux/dbSlice';

export default class ManageLicense {
	constructor(licenseKey) {
		this.licenseKey = licenseKey.trim();
		// eslint-disable-next-line no-constructor-return
		return this.startValidation();
	}

	async startValidation() {
		const request = await this.validateKey();
		if (request.status === 200) {
			this.activate(request);
		}
		return request.status;
	}

	activate(request) {
		store.dispatch(setExpiration(request.expiration));
		store.dispatch(setMAC(request.mac));
	}

	async validateKey() {
		let response;
		await $.ajax({
			beforeSend: (xhr) => {
				xhr.setRequestHeader('Authorization', config.AUTH);
			},
			url: config.REST,
			type: 'PUT',
			data: {
				action: 1,
				licenseKey: this.licenseKey,
				macAddress: window.MAC
			},
			success: async (res, textStatus, xhr) => {
				response = { status: await xhr.status, expiration: res, mac: window.MAC };
			}
		}).catch((err) => {
			response = { status: err.status };
		});
		return response;
	}
}
