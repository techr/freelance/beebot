const getTrophyStatuses = (lessonID) => {
	const trophies = [
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false
	];
	trophies.fill(true, 0, lessonID);
	return trophies;
};

export default (lessonID) => getTrophyStatuses(lessonID);
