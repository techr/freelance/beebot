import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import LicenseManagment from '../controllers/LicenseManagment';
import Container from '../components/Container';
import '../sass/activation.scss';

export default () => {
	const navigate = useNavigate();
	const [key, setKey] = useState('');
	const [wrongKey, setWrongKey] = useState(false);
	const [technicalError, setTechnicalError] = useState(false);

	const getErrorMessage = () => (
		<span className={'red'}>Възникна технически проблем, опитайте пак.</span>
	);

	const getWrongKeyMessage = () => <span className={'red'}>Въведеният код е грешен.</span>;

	const change = (event) => {
		setKey(event.target.value);
	};

	const submit = async () => {
		const result = await new LicenseManagment(key);
		if (result === 200) {
			navigate('/Home');
		}

		switch (result) {
			case 406:
				setWrongKey(true);
				break;
			default:
				setTechnicalError(true);
		}
		return false;
	};
	return (
		<Container
			class={'activation'}
			content={
				<div className={'white-box big'}>
					<h2>Моля активирайте продукта</h2>
					<input
						name={'key'}
						type={'text'}
						placeholder={'Лицензен ключ'}
						value={key}
						onChange={change}
					/>
					<div role={'button'} tabIndex={'0'} className={'activate'} onClick={submit}>
						Активирай
					</div>
					<div className={'result'}>
						{technicalError ? getErrorMessage() : null}
						{wrongKey ? getWrongKeyMessage() : null}
					</div>
				</div>
			}
		/>
	);
};
