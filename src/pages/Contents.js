import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import Container from '../components/Container';
import '../sass/contents.scss';
import getText from '../controllers/getText';
import { setLessonID } from '../redux/dbSlice';

export default () => {
	const navigate = useNavigate();
	const dispatch = useDispatch();
	const [text, setText] = useState(undefined);
	const getContents = (lessons) =>
		lessons.map((lesson, index) => (
			<div
				role={'button'}
				tabIndex={'0'}
				className={'pointer item'}
				onClick={() => {
					dispatch(setLessonID(index));
					navigate('/Game');
				}}
			>{`${index + 1}. ${lesson.title}`}</div>
		));

	useEffect(() => {
		getText().then((result) => setText(result));
	}, []);

	return text ? (
		<Container
			class={'contents'}
			content={
				<div className={'white-box big'}>
					<div
						className={'homeButton pointer'}
						role={'button'}
						tabIndex={'0'}
						onClick={() => navigate('/Home')}
					>
						<i className={'material-icons'}>home</i>
					</div>
					{getContents(text.lessons)}
				</div>
			}
		/>
	) : (
		<div />
	);
};
