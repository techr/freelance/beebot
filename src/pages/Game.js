import React, { useEffect, useState, useRef, useCallback } from 'react';
import { useDrag, useDrop } from 'react-dnd';
import { useNavigate } from 'react-router-dom';
import Redo from '@mui/icons-material/Redo';
import Undo from '@mui/icons-material/Undo';
import ArrowDownward from '@mui/icons-material/ArrowDownward';
import ArrowUpward from '@mui/icons-material/ArrowUpward';
import { useSelector, useDispatch } from 'react-redux';
import Sketchpad from '../controllers/Sketchpad';
import BeeMovement from '../controllers/BeeMovement';
import getText from '../controllers/getText';
import Lesson from '../components/Lesson';
import turquoise from '../img/game/drawing/colors/turquoise.png';
import emerald from '../img/game/drawing/colors/emerald.png';
import peterRiver from '../img/game/drawing/colors/peter-river.png';
import amethyst from '../img/game/drawing/colors/amethyst.png';
import wetAsphalt from '../img/game/drawing/colors/wet-asphalt.png';
import sunFlower from '../img/game/drawing/colors/sun-flower.png';
import carrot from '../img/game/drawing/colors/carrot.png';
import alizarin from '../img/game/drawing/colors/alizarin.png';
import cloud from '../img/game/drawing/colors/cloud.png';
import concrete from '../img/game/drawing/colors/concrete.png';
import pencil from '../img/game/drawing/tools/pencil.png';
import eraser from '../img/game/drawing/tools/eraser.png';
import smallSize from '../img/game/drawing/tools/small-size.png';
import bigSize from '../img/game/drawing/tools/big-size.png';
import '../sass/main.scss';
import '../sass/game.scss';
import { setNextPage } from '../redux/dbSlice';

const Game = () => {
	const navigate = useNavigate();
	const dispatch = useDispatch();
	const [text, setText] = useState(undefined);
	const [backgrMusicIcon, setBackgrMusicIcon] = useState('volume_up');
	const [beeInvisibility, setBeeInvisibility] = useState('');
	const [pressedButtons, setPressedButtons] = useState([]);
	const [sketchpad, setSketchpad] = useState({});
	const [controller, setController] = useState({});
	const embed = React.createRef();
	const beeImageName = useSelector((state) => state.character);
	const lessonID = useSelector((state) => state.lessonID);
	const dropCoordinates = { x: 0, y: 0 };
	dispatch(setNextPage(''));

	const [hoveringBeeLeft, setHoveringBeeLeft] = useState(-200);
	const [hoveringBeeTop, setHoveringBeeTop] = useState(-200);

	const [{ isDragging }, drag] = useDrag(() => ({
		type: '1',
		item: { name: '1' },
		end: (item, monitor) => {
			const dropResult = monitor.getDropResult();
			if (item && dropResult) {
				// alert(`You dropped ${item.name} into ${dropResult.name}!`);
			}
		},
		collect: (monitor) => ({
			isDragging: monitor.isDragging(),
			handlerId: monitor.getHandlerId(),
			opacity: monitor.isDragging() ? 0.4 : 1
		})
	}));

	let opacity = 1;

	if (beeInvisibility === '') {
		opacity = isDragging ? 0.4 : 1;
	} else {
		opacity = 0;
	}

	const boundingBox = useRef(null);

	const [, drop] = useDrop(
		() => ({
			accept: '1',
			hover: (item, monitor) => {
				const offset = monitor.getClientOffset();
				dropCoordinates.x = offset.x - boundingBox.current.x;
				dropCoordinates.y = offset.y - boundingBox.current.y;
				setHoveringBeeTop(`${offset.y - 5}px`);
				setHoveringBeeLeft(`${offset.x - 19}px`);
			},
			drop: () => {
				controller.insertImage(controller.getBoxAndRowID(dropCoordinates));
				setBeeInvisibility('invisible');
			},
			collect: (monitor) => ({
				isOver: monitor.isOver(),
				canDrop: monitor.canDrop()
			})
		}),
		[controller]
	);

	function combinedRef(el) {
		drop(el);
		if (el) {
			boundingBox.current = el.getBoundingClientRect();
		}
	}

	const clearTheBoard = () => {
		if (!sketchpad.clear || !controller.reset) return;
		sketchpad.clear();
		controller.reset();
		setHoveringBeeLeft(-200);
		setHoveringBeeTop(-200);
		setBeeInvisibility('');
		setPressedButtons([]);
	};

	useEffect(() => {
		clearTheBoard();
	}, [lessonID]);

	const onControlClick = (e) => {
		e.persist();
		const actionID = parseInt(e.target.getAttribute('data-action'), 10);
		if (actionID !== 7 && actionID !== 6) {
			setPressedButtons((prevState) => [...prevState, actionID]);
			controller.pushSteps(actionID);
		} else if (actionID === 6) {
			controller.runTheSteps();
		} else {
			clearTheBoard();
		}
		e.target.setAttribute('style', 'transform: translate(0px, 5px);');
		setTimeout((target) => target.setAttribute('style', ''), 100, e.target);
	};

	const getArrowLabel = (button) => {
		switch (button) {
			case 1:
				return <ArrowUpward />;
			case 2:
				return <Redo />;
			case 3:
				return <ArrowDownward />;
			case 4:
				return <Undo />;
			default:
				return '';
		}
	};

	const toggleBackgroundMusic = () => {
		if (!window.music.paused) {
			setBackgrMusicIcon('volume_off');
			window.music.pause();
		} else {
			setBackgrMusicIcon('volume_up');
			window.music.play();
		}
	};

	const initiateDrawing = () => {
		setSketchpad(
			new Sketchpad({
				element: '#drawing-board',
				scale: 0.92
			})
		);
	};

	const loadBeeMovement = (elName) => {
		setController(new BeeMovement(elName, beeImageName, text.lessons[lessonID].images));
	};

	const initiateGameUI = useCallback(() => {
		const elName = '#svgEmbed';
		document.querySelector(elName).addEventListener('load', () => {
			loadBeeMovement(elName);
		});
		const beeBoard = document.querySelector(elName);
		if (beeBoard && beeBoard.getSVGDocument()) {
			loadBeeMovement(elName);
		}
	}, [lessonID, text, beeImageName]);

	const renderPressedButtons = (buttons) => {
		if (beeInvisibility) {
			return buttons.map((button) => <i className={'material-icons'}>{getArrowLabel(button)}</i>);
		}
		return null;
	};

	useEffect(() => {
		if (text) {
			initiateDrawing(lessonID);
			initiateGameUI(lessonID);
		}
	}, [text, lessonID, beeImageName]);
	useEffect(() => {
		getText().then((result) => setText(result));
	}, []);

	return text ? (
		<div>
			<div className={'orange-line'} />
			<header>
				<nav>
					<div className={'item'} role={'button'} tabIndex={'0'} onClick={() => navigate('/Home')}>
						<i className={'material-icons'}>home</i>
						<span>Начало</span>
					</div>
					<div
						className={'item'}
						role={'button'}
						tabIndex={'0'}
						onClick={() => navigate('/Contents')}
					>
						<i className={'material-icons'}>flag</i>
						<span>Съдържание</span>
					</div>
					<div
						className={'item'}
						role={'button'}
						tabIndex={'0'}
						onClick={() => window.electronAPI.openLink('https://bdp.innovateconsult.net')}
					>
						<i className={'material-icons'}>settings</i>
						<span>За играта</span>
					</div>
					<div
						className={'item'}
						role={'button'}
						tabIndex={'0'}
						onClick={() => navigate('/Contacts')}
					>
						<i className={'material-icons'}>phone</i>
						<span>Контакти</span>
					</div>
				</nav>
				<div
					className={'background-sound-button'}
					role={'button'}
					tabIndex={'0'}
					onClick={toggleBackgroundMusic}
				>
					<i className={'material-icons'}>{backgrMusicIcon}</i>
				</div>
			</header>

			<div className={'container'}>
				<div className={'left'}>
					<Lesson />
					<div className={'game-managment'}>
						<div className={'white-box bee-controls'}>
							<div>
								<i
									role={'button'}
									tabIndex={'0'}
									onClick={onControlClick}
									className={'material-icons'}
									data-action={'1'}
								>
									<ArrowUpward />
								</i>
							</div>
							<div>
								<i
									role={'button'}
									tabIndex={'0'}
									onClick={onControlClick}
									className={'material-icons'}
									data-action={'4'}
								>
									<Undo />
								</i>
								<span
									role={'button'}
									tabIndex={'0'}
									className={'go-button'}
									onClick={onControlClick}
									data-action={'6'}
								>
									GO
								</span>
								<i
									role={'button'}
									tabIndex={'0'}
									onClick={onControlClick}
									className={'material-icons'}
									data-action={'2'}
								>
									<Redo />
								</i>
							</div>
							<div>
								<i
									role={'button'}
									tabIndex={'0'}
									className={'material-icons'}
									data-action={'7'}
									onClick={onControlClick}
								>
									clear
								</i>
								<i
									role={'button'}
									tabIndex={'0'}
									className={'material-icons'}
									onClick={onControlClick}
									data-action={'3'}
								>
									<ArrowDownward />
								</i>
								<i
									role={'button'}
									tabIndex={'0'}
									className={'material-icons'}
									onClick={onControlClick}
									data-action={'5'}
								>
									pause
								</i>
							</div>
						</div>
						<div className={'white-box board-manager'}>
							<div className={'colors'}>
								<div>
									<img
										role={'button'}
										src={turquoise}
										alt={'Tturquoise'}
										onClick={() => {
											sketchpad.color = '#1abc9c';
										}}
									/>
									<img
										role={'button'}
										src={emerald}
										alt={'Emerald'}
										onClick={() => {
											sketchpad.color = '#2ecc71';
										}}
									/>
									<img
										role={'button'}
										src={peterRiver}
										alt={'Peter River'}
										onClick={() => {
											sketchpad.color = '#3498db';
										}}
									/>
									<img
										role={'button'}
										src={amethyst}
										alt={'Amethyst'}
										onClick={() => {
											sketchpad.color = '#9b59b6';
										}}
									/>
									<img
										role={'button'}
										src={wetAsphalt}
										alt={'Wet Asphalt'}
										onClick={() => {
											sketchpad.color = '#34495e';
										}}
									/>
								</div>
								<div>
									<img
										role={'button'}
										src={sunFlower}
										alt={'Sun Flower'}
										onClick={() => {
											sketchpad.color = '#f1c40f';
										}}
									/>
									<img
										role={'button'}
										src={carrot}
										alt={'Carrot'}
										onClick={() => {
											sketchpad.color = '#e67e22';
										}}
									/>
									<img
										role={'button'}
										src={alizarin}
										alt={'Alizarin'}
										onClick={() => {
											sketchpad.color = '#e74c3c';
										}}
									/>
									<img
										role={'button'}
										src={cloud}
										alt={'Cloud'}
										onClick={() => {
											sketchpad.color = '#ecf0f1';
										}}
									/>
									<img
										role={'button'}
										src={concrete}
										alt={'Concrete'}
										onClick={() => {
											sketchpad.color = '#95a5a6';
										}}
									/>
								</div>
							</div>
							<div className={'tools'}>
								<span>
									<img src={pencil} data-mode={'pencil'} alt={'Pencil'} />
									<img
										role={'button'}
										src={eraser}
										data-mode={'eraser'}
										alt={'Eraser'}
										onClick={() => sketchpad.clear()}
									/>
								</span>
								<span>
									<img
										role={'button'}
										src={smallSize}
										alt={'Small'}
										onClick={() => {
											sketchpad.penSize = 5;
										}}
									/>
									<img
										role={'button'}
										src={bigSize}
										alt={'Big'}
										onClick={() => {
											sketchpad.penSize = 10;
										}}
									/>
								</span>
							</div>
						</div>

						<div className={`white-box bee-container ${beeInvisibility ? 'flexStart' : ''}`}>
							<img
								className={beeInvisibility}
								ref={drag}
								style={{ opacity }}
								data-testid={'1'}
								src={`${process.env.PUBLIC_URL}/img/bees/regular/${beeImageName}`}
								alt={'Bot'}
							/>
							<div>{renderPressedButtons(pressedButtons)}</div>
						</div>
					</div>
				</div>
				<div className={'right'}>
					<div className={'board-container'}>
						<embed
							id={'svgEmbed'}
							ref={embed}
							className={'white-box board'}
							src={`${process.env.PUBLIC_URL}/img/board.svg`}
							alt={'Board'}
						/>
						<canvas ref={combinedRef} className={'drawing-board'} id={'drawing-board'} />
					</div>
				</div>
				<img
					className={`${beeInvisibility} hoveringBee`}
					style={{
						display: opacity ? 'block' : 'none',
						opacity,
						top: hoveringBeeTop,
						left: hoveringBeeLeft
					}}
					data-testid={'1'}
					src={`${process.env.PUBLIC_URL}/img/bees/regular/${beeImageName}`}
					alt={'Bot'}
				/>
			</div>
		</div>
	) : (
		<div />
	);
};

export default Game;
