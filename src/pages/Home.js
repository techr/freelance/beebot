import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import NavigateNext from '@mui/icons-material/NavigateNext';
import NavigateBefore from '@mui/icons-material/NavigateBefore';
import { useSelector, useDispatch } from 'react-redux';
import config from '../config';
import Container from '../components/Container';
import langBG from '../img/home/bg.png';
import langEN from '../img/home/us.png';
import eighthBot from '../img/home/bees/8.png';
import seventhBot from '../img/home/bees/7.png';
import sixthBot from '../img/home/bees/6.png';
import fifthBot from '../img/home/bees/5.png';
import fourthBot from '../img/home/bees/4.png';
import thirdBot from '../img/home/bees/3.png';
import blueBot from '../img/home/bees/2.png';
import beeBot from '../img/home/bees/1.png';
import contents from '../img/home/contents.png';
import contacts from '../img/home/contacts.png';
import getText from '../controllers/getText';
import '../sass/home.scss';
import { setNextPage, setLang, setCharacter } from '../redux/dbSlice';

const Home = () => {
	const navigate = useNavigate();
	const dispatch = useDispatch();
	const [text, setText] = useState(undefined);
	const character = useSelector((state) => state.character);
	const lang = useSelector((state) => state.lang);
	useEffect(() => {
		dispatch(setNextPage(''));
	}, []);
	useEffect(() => {
		getText().then((result) => {
			setText(result);
		});
	}, []);

	const { beePictures } = config;

	const getCharacterPictureRef = (imageName) => {
		switch (imageName) {
			case '1.png':
				return beeBot;
			case '2.png':
				return blueBot;
			case '3.png':
				return thirdBot;
			case '4.png':
				return fourthBot;
			case '5.png':
				return fifthBot;
			case '6.png':
				return sixthBot;
			case '7.png':
				return seventhBot;
			case '8.png':
				return eighthBot;
			default:
				return beeBot;
		}
	};

	const getLangPicture = () => {
		if (lang !== null && lang === 'en') {
			return langEN;
		}
		return langBG;
	};

	const [langPicture, setLangPicture] = useState(getLangPicture());
	const [beeImageName, setBeeImageName] = useState(character);
	const [botPicture, setBotPicture] = useState(getCharacterPictureRef(beeImageName));
	const [langOpt, setLangOpt] = useState(lang !== 'bg' && lang);

	const switchLang = () => {
		if (lang === 'bg') {
			dispatch(setLang('en'));
			setLangPicture(langEN);
			dispatch(setLang('en'));
			setLangOpt(true);
			dispatch(setNextPage('/Home'));
			// this.forceUpdate();
		} else {
			dispatch(setLang('bg'));
			setLangPicture(langBG);
			dispatch(setLang('bg'));
			setLangOpt(false);
			dispatch(setNextPage('/Home'));
			// this.forceUpdate();
		}
	};

	const nextBot = () => {
		const nextImageName = beePictures[beeImageName].next;
		dispatch(setCharacter(nextImageName));
		setBotPicture(getCharacterPictureRef(nextImageName));
		setBeeImageName(nextImageName);
	};

	const prevBot = () => {
		const prevImageName = beePictures[beeImageName].prev;
		dispatch(setCharacter(prevImageName));
		setBotPicture(getCharacterPictureRef(prevImageName));
		setBeeImageName(prevImageName);
	};
	return text ? (
		<Container
			class={'home'}
			content={
				<div className={'white-box big'}>
					<div className={'topRow'}>
						<div
							className={'play'}
							role={'button'}
							tabIndex={'0'}
							onClick={() => navigate('/Game')}
						>
							<i className={'material-icons'} data-action={'1'}>
								play_arrow
							</i>
							<div>{text.ui.home[0]}</div>
						</div>
						<div
							className={'contents'}
							role={'button'}
							tabIndex={'0'}
							onClick={() => navigate('/Contents')}
						>
							<img src={contents} alt={'Contents'} />
						</div>
					</div>
					<div className={'bottomRow'}>
						<div className={'box'} role={'button'} tabIndex={'0'} onClick={nextBot}>
							<div className={'changableBee'}>
								<i
									role={'button'}
									tabIndex={'0'}
									className={'material-icons'}
									data-action={'7'}
									onClick={prevBot}
								>
									<NavigateBefore fontSize={'large'} />
								</i>
								<img src={botPicture} alt={'Пчела'} />
								<i
									role={'button'}
									tabIndex={'0'}
									className={'material-icons'}
									data-action={'7'}
									onClick={nextBot}
								>
									<NavigateNext fontSize={'large'} />
								</i>
							</div>
							<span>{text.ui.home[1]}</span>
						</div>
						<div className={'box langSwitch'} role={'button'} tabIndex={'0'} onClick={switchLang}>
							<div className={'switchContainer'}>
								<label className={'switch'} htmlFor={'lang'}>
									<input type={'checkbox'} id={'lang'} onChange={switchLang} checked={langOpt} />
									<span className={'slider round'} />
								</label>
							</div>
							<img src={langPicture} alt={'Български'} />
							<span>{text.ui.home[2]}</span>
						</div>
						<div
							className={'box'}
							role={'button'}
							tabIndex={'0'}
							onClick={() => navigate('/Contacts')}
						>
							<img src={contacts} alt={'Контакти'} />
							<span>{text.ui.home[3]}</span>
						</div>
					</div>
				</div>
			}
		/>
	) : (
		<div />
	);
};
export default Home;
