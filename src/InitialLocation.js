import React from 'react';
import { Navigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import getMAC from './controllers/getMAC';

export default () => {
	window.MAC = getMAC();
	if (
		useSelector((state) => state.expiration) !== null &&
		new Date(useSelector((state) => state.expiration)) >= new Date() &&
		useSelector((state) => state.mac) === window.MAC
	) {
		return <Navigate to={'/Home'} />;
	}

	return <Navigate to={'/Activation'} />;
};
