export default {
	REST: 'https://api-bdp.innovateconsult.net',
	AUTH: 'Basic YXBwOk9iNnRhdGFwYXJvbEFhNG8==',
	beePictures: {
		'1.png': {
			prev: '8.png',
			next: '2.png'
		},
		'2.png': {
			prev: '1.png',
			next: '3.png'
		},
		'3.png': {
			prev: '2.png',
			next: '4.png'
		},
		'4.png': {
			prev: '3.png',
			next: '5.png'
		},
		'5.png': {
			prev: '4.png',
			next: '6.png'
		},
		'6.png': {
			prev: '5.png',
			next: '7.png'
		},
		'7.png': {
			prev: '6.png',
			next: '8.png'
		},
		'8.png': {
			prev: '7.png',
			next: '1.png'
		}
	}
};
