import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import config from './config';
import App from './App';
// eslint-disable-next-line import/no-named-as-default-member
import reportWebVitals from './reportWebVitals';
import { persistor, store, getManualRehydration } from './redux/store';
import { setCharacter } from './redux/dbSlice';

document.title = 'Безопастност на движението по пътищата';
window.music = new Audio(`${process.env.PUBLIC_URL}/sounds/background.mp3`);
window.music.loop = true;
window.music.play();
getManualRehydration()
	.then((rehydration) => store.dispatch(rehydration))
	.then(() => {
		if (!config.beePictures[store.getState().character]) {
			store.dispatch(setCharacter('1.png'));
		}
	});

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
	<React.StrictMode>
		<Provider store={store}>
			<PersistGate loading={null} persistor={persistor}>
				<App />
			</PersistGate>
		</Provider>
	</React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
