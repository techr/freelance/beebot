# A React-Electron Windows application
- demo can be seen here - https://youtu.be/VGTZNNMcHfk
- designed to be multilingual
- includes manually modifying a custom SVG for
	- adding pictures
	- drag and drop character aditions
- it is designed to run in a 1280x720 window (automatically set through Electron), the UI was not designed and developed for use at different resolutions
- if an antivirus is enabled it takes a while to load
- the throphies on the top page of the bee movement screen allow changing the current lesson/pictures/tasks on the page
- has a very simple, not designed to be hacker proof, license management system (currently disabled from InitialLocation.js by swapping the paths)

## Improvements
- presently the application does not have any written tests, it should incorporate E2E tests
	- I would make some integration tests with the backend that would test whether we have a connection, whether it responds as expected whenever a valid/invalid MAC address is passed
	- The UI tests should test each component individually, especially the ones with props, whole UI test shouldbe completed of the whole app
	- some unit tests could be checking the current lessonID from the local storage after change, checking whether the MAC address was set, checking the bee position and orientation after a speicific sequence of movements (test edges of the board)
- the authentication token in the config file would probably be best fetched from the backend before starting
- not all images should be drag and dropable on the game screen for better UX

# Instructions
Start in dev mode (runs an Electron.js browser):
```
npm i
npm run dev
```

Build executable
- must change the paths in ./src/controllers/BeeMovement.js as directed in the comments there
- Electron forge may require additional software to be installed for packaging - https://www.electronforge.io/
```
npm i
npm run package
```