const { contextBridge, ipcRenderer } = require('electron');

contextBridge.exposeInMainWorld('electronAPI', {
	openLink: (link) => ipcRenderer.send('open-link', link)
});
